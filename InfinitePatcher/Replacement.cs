﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfinitePatcher
{
    public class Replacement
    {
        private byte[] _bytes;

        private int _index = 0;
        private int _start = 0;

        public Replacement(byte[] bytes)
        {
            _bytes = bytes;
        }

        public int Check(byte b, int byteIndex)
        {
            if (_bytes[_index] == b)
            {
                if (_index == 0)
                {
                    _start = byteIndex;
                }

                _index++;

                if (_index == _bytes.Length)
                {
                    // found GameCms
                    _index = 0;
                    return _start;
                }
            }
            else
            {
                _index = 0;
            }
            return -1;
        }

        public void Replace(int location, ref byte[] bytes)
        {
            Console.WriteLine("Replaced at " + location);
            for (int i = 0; i < _bytes.Length; i++)
            {
                bytes[location + i] = 0x00;
            }
        }
    }
}
