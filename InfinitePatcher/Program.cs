﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfinitePatcher
{
    public class Program
    {
        private static List<Replacement> _replacements = new List<Replacement>();

        [STAThread]
        private static void Main(string[] args)
        {
            _replacements.Add(new Replacement(new byte[] { 0x47, 0x61, 0x6D, 0x65, 0x43, 0x6D, 0x73 }));
            _replacements.Add(new Replacement(new byte[] { 0x67, 0x61, 0x6D, 0x65, 0x63, 0x6D, 0x73 }));


            Console.WriteLine("Starting Patcher!");
            try
            {
                Console.WriteLine(Patch());
            }
            catch (FileLoadException e)
            {
                Console.WriteLine("Unhandled Exception: " + e.ToString());
            }
            finally
            {
                Console.WriteLine("Close window by hitting any key");
            }

            Console.ReadKey();
        }

        private static string Patch()
        {
            Console.WriteLine("Select your *.exe");
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return "No file selected, exiting";
            }

            var path = ofd.FileName;
            var fileBytes = File.ReadAllBytes(path);
            
            var outBytes = fileBytes;

            for (var i = 0; i < fileBytes.Length; i++)
            {
                
                foreach (var replacement in _replacements)
                {
                    var pos = replacement.Check(fileBytes[i], i);
                    if(pos == -1) continue;
                    replacement.Replace(pos, ref outBytes);
                }

            }

            File.WriteAllBytes(path, outBytes);
            return "Done";
        }

        
    }
}
